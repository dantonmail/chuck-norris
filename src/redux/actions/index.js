import {
  CATEGORY_REQUEST_STARTED,
  CATEGORY_REQUEST_SUCCESS,
  CATEGORY_REQUEST_FAILURE,
  JOKE_REQUEST_STARTED,
  JOKE_REQUEST_SUCCESS,
  JOKE_REQUEST_FAILURE,
} from '../types';
import axios from 'axios';
//обработка запроса категорий
export const categoryRequestStarted = () => ({
  type: CATEGORY_REQUEST_STARTED,
});

export const categoryRequestSuccess = (categories) => ({
  type: CATEGORY_REQUEST_SUCCESS,
  payload: categories,
});

export const categoryRequestFailure = (error) => ({
  type: CATEGORY_REQUEST_FAILURE,
  payload: error,
});

// ============
//обработка запроса добавление шутки
export const jokeRequestStarted = () => ({
  type: JOKE_REQUEST_STARTED,
});

export const jokeRequestSuccess = (joke) => ({
  type: JOKE_REQUEST_SUCCESS,
  payload: joke,
});

export const jokeRequestFailure = (error) => ({
  type: JOKE_REQUEST_FAILURE,
  payload: error,
});

//запрос категорий
export const categoryRequest = () => {
  return (dispatch) => {
    dispatch(categoryRequestStarted());

    axios
      .get('https://api.chucknorris.io/jokes/categories', [])
      .then((res) => {
        dispatch(categoryRequestSuccess(res.data));
      })
      .catch((err) => {
        dispatch(categoryRequestFailure(err.message));
      });
  };
};

//запрос рандомной шутки
export const jokeRequestRandom = () => {
  return (dispatch) => {
    dispatch(jokeRequestStarted());

    axios
      .get('https://api.chucknorris.io/jokes/random', [])
      .then((res) => {
        dispatch(jokeRequestSuccess(res.data));
      })
      .catch((err) => {
        dispatch(jokeRequestFailure(err.message));
      });
  };
};

//запрос шутки по категории
export const jokeRequest = (data) => {
  return (dispatch) => {
    dispatch(jokeRequestStarted());

    axios
      .get(`https://api.chucknorris.io/jokes/random?category=${data}`, [])
      .then((res) => {
        dispatch(jokeRequestSuccess(res.data));
      })
      .catch((err) => {
        dispatch(jokeRequestFailure(err.message));
      });
  };
};
