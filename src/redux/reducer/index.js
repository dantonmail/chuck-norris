import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import {
  CATEGORY_REQUEST_STARTED,
  CATEGORY_REQUEST_SUCCESS,
  CATEGORY_REQUEST_FAILURE,
  JOKE_REQUEST_STARTED,
  JOKE_REQUEST_SUCCESS,
  JOKE_REQUEST_FAILURE,
} from '../types';

const initialStore = {
  data: [],
  categories: [],
};

function rootReducer(store = initialStore, action) {
  switch (action.type) {
    // ======== добавление категорий
    case CATEGORY_REQUEST_STARTED:
      return {
        ...store,
        loading: true,
      };
    case CATEGORY_REQUEST_SUCCESS:
      return {
        ...store,
        loading: false,
        categories: action.payload,
      };
    case CATEGORY_REQUEST_FAILURE:
      return {
        ...store,
        loading: false,
        error: action.payload,
      };
    // ======== добавление шутки
    case JOKE_REQUEST_STARTED:
      return {
        ...store,
        loading: true,
      };
    case JOKE_REQUEST_SUCCESS:
      return {
        ...store,
        loading: false,
        data: action.payload,
      };
    case JOKE_REQUEST_FAILURE:
      return {
        ...store,
        loading: false,
        error: action.payload,
      };
    default:
      return store;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
