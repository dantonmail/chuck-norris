import Card from '../Card';
import React from 'react';

function Itemlist({ category = [] }) {
  return (
    <>
      {category.map((item, index) => {
        return <Card category={item} key={index} />;
      })}
    </>
  );
}

export default Itemlist;
