import React from 'react';
import Itemlist from '../Itemlist';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { categoryRequest } from '../../redux/actions';

function Itempage() {
  const dispatch = useDispatch();
  //запрс категорий на сервере
  useEffect(() => {
    dispatch(categoryRequest());
  }, [dispatch]);

  //получение категорий из Сторы
  const categories = useSelector((store) => {
    return store.categories;
  });
  return <Itemlist category={categories} />;
}
export default Itempage;
