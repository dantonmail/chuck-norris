import '../Header/style.scss';
import Headerlogo from '../Headerlogo';

function Header() {
  return (
    <header className='header'>
      <div className='container container__header'>
        <div className='header-topmenu'>
          <Headerlogo />
        </div>
      </div>
    </header>
  );
}
export default Header;
