import React from 'react';
import { useSelector } from 'react-redux';

function Headerlogo() {
  const data = useSelector((store) => {
    return store.data;
  });
  const { icon_url } = data;
  return (
    <div className='header-topmenu__logo'>
      <img
        className='header-topmenu__logo__icon'
        src={icon_url}
        alt='just logo'
      />
      <span className='header-topmenu__logo__text'>Chuck Norris</span>
    </div>
  );
}

export default Headerlogo;
