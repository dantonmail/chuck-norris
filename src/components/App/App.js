import React from 'react';
import Itempage from '../Itempage';
import Header from '../Header';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { jokeRequestRandom } from '../../redux/actions';
import Button from '../Button';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(jokeRequestRandom());
  }, [dispatch]);

  const data = useSelector((store) => {
    return store.data;
  }); //получение данных из store
  return (
    <div className='App'>
      <div className='main__container'>
        <Header />
        <div className='page__container'>
          <h1 className='page__title'>Categories</h1>
          <div className='cards'>
            <Itempage />
            <Button
              text='random'
              bgc='#FFFFFF'
              onClick={() => {
                dispatch(jokeRequestRandom()); //вызов следующей шутки
              }}
              btnClName='button card__button'
            />
          </div>
          <div className='page__text'>{data.value}</div>
          <div className='page__img'>
            <img
              src='https://res.cloudinary.com/dllgerabj/image/upload/v1629488520/goodfood/chuck%20norris/chucknorris_mplzfz.png'
              alt='Chuck Norris'
            />
          </div>
        </div>
      </div>
    </div>
  );
}
export default App;
