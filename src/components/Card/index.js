import '../../_variables.scss';
import Button from '../Button';
import React from 'react';
import { useDispatch } from 'react-redux';
import { jokeRequest } from '../../redux/actions';

function Card({ category: categories = [] }) {
  const dispatch = useDispatch();
  return (
    <Button
      text={categories}
      bgc='#FFFFFF'
      onClick={() => {
        dispatch(jokeRequest(categories)); //запос на вызов следующей шутки из соответсвующей категории
      }}
      btnClName='button '
    />
  );
}

export default Card;
